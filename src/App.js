import * as THREE from "three";
import { Spin } from "antd";
import React, { useRef, Suspense } from "react";
import {
  Canvas,
  useThree,
  useFrame,
  useLoader,
  extend,
  Dom,
} from "react-three-fiber";

import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { OBJLoader2 } from "three/examples/jsm/loaders/OBJLoader2";
import { MTLLoader } from "three/examples/jsm/loaders/MTLLoader";
import "antd/dist/antd.css";

extend({ OrbitControls });

const Controls = (props) => {
  const { gl, camera } = useThree();
  return <orbitControls args={[camera, gl.domElement]} {...props} />;
};

const MeshBundle = ({ obj, mtl, image }) => {
  const mesh = useRef();
  const materialLoaded = useLoader(MTLLoader, mtl);
  const texture = useLoader(THREE.TextureLoader, image);
  const objLoaded = useLoader(OBJLoader2, obj);

  return (
    <mesh
      ref={mesh}
      materials={materialLoaded.materials}
      position={[0, 0, -200]}
    >
      <ambientLight intensity={0.4} />
      <pointLight position={[50, 50, 50]} intensity={1} />
      <primitive object={objLoaded} />
      <meshBasicMaterial map={texture} />
    </mesh>
  );
};

const App = () => (
  <Canvas id="canvas" concurrent>
    <Suspense
      fallback={
        <Dom className="bg">
          <Spin
            tip="Loading..."
            style={{ color: "#fff", fontSize: "20px", fontWeight: 600 }}
          />
        </Dom>
      }
    >
      <MeshBundle
        obj="/data/mesh-bundle/Stadsbader_simplified_3d_mesh.obj"
        mtl="/data/mesh-bundle/Stadsbader_simplified_3d_mesh.mtl"
        image="/data/mesh-bundle/Stadsbader_texture.jpg"
      />
    </Suspense>

    <Controls
      autoRotate
      enablePan
      enableZoom
      enableDamping
      dampingFactor={0.5}
      rotateSpeed={1}
      maxPolarAngle={Math.PI / 2}
      minPolarAngle={Math.PI / 2}
    />
  </Canvas>
);

export default App;
